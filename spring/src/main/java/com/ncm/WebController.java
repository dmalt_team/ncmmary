package com.ncm;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class WebController {
	
	@Value("${spring.scriptVersion}")
	private String scriptVersion;

	@Autowired
	private Environment environment;

	boolean isDev = false;
	String currentAddress = "";
	
	static List<Locale> LOCALES = Arrays.asList(new Locale("en"), new Locale("ko")); 

	@PostConstruct
	public void postConstruct() {

		// get profile
		for (String profile : environment.getActiveProfiles()) {
			if (profile.equals("dev")) isDev = true;
		}

		// get server address
		try {
			if (isDev) currentAddress = "http://"+InetAddress.getLocalHost().getHostAddress()+":8080";
			else currentAddress = "https://edgeoftheworld.net";
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}

	//

	@RequestMapping(value = {"/", ""})
	public String home(@RequestParam(value = "lang", required = false) String lang, HttpServletRequest request) {
		if (lang != null) {
			if (lang.equals("ko")) {
				return "/index.ko";
			} else {
				return "/index";
			}
		} else {
			if (request.getLocale().getLanguage().equals("ko")) {
				return "/index.ko";
			} else {
				return "/index";
			}
		}
	}

}
