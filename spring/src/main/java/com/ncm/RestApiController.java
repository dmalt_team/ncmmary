package com.ncm;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ncm.content.models.Content;
import com.ncm.content.repositories.ContentRepository;

@CrossOrigin(origins="*", maxAge=3600)
@RestController
@RequestMapping("/api")
public class RestApiController {
	
	@Autowired
	ContentRepository contentRepo;
	
	static long ONE_DAY = 86400000L;
	
	//
	
	/**
	 * 앱 -> 기기로 요청 할 떄 쓰는 API
	 * wait 값이 true 일 경우, 해당 처리를 실제 아두이노 기계에서 처리하도록 기다리고,
	 * wait 값이 false 일 경우, 바로 return 하고 끝난다.
	 */
	@PostMapping("/cube_request")
	public ResponseEntity<Content> requestToCube(@RequestBody String requestStr) {
		
		// 
		JSONObject request = new JSONObject(requestStr);
		
		long currentTimestamp = System.currentTimeMillis();
		String type = request.getString("type");
		boolean isWaitRequestComplete = request.getBoolean("wait");
		
		Content newRequest = new Content();
		newRequest.type = "cube";
		newRequest.requestType = type;
		newRequest.status = 0L;
		newRequest.createdTime = currentTimestamp;
		newRequest.lastModifiedTime = currentTimestamp;
		newRequest = contentRepo.save(newRequest);
		
		// audio request
		Content audioRequest = new Content();
		audioRequest.type = "cube";
		audioRequest.requestType = "door_open_audio";
		audioRequest.status = 0L;
		audioRequest.createdTime = currentTimestamp;
		audioRequest.lastModifiedTime = currentTimestamp;
		audioRequest = contentRepo.save(audioRequest);
		
		if (isWaitRequestComplete) {
			Content requestedData;
			boolean isComplete = false;
			
			try {
				
				do {
					
					Thread.sleep(1000); // 1초에 한번씩 체크한다
					
					requestedData = contentRepo.findById(newRequest.id);
					isComplete = requestedData.status == 1L;
					
					System.out.println("Wait ... id="+Long.toString(newRequest.id));
					
				} while(isComplete);
				
				return new ResponseEntity<Content>(requestedData, HttpStatus.OK);
				 
			} catch(Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Content>(HttpStatus.BAD_GATEWAY);
			}
			
		} else {
			return new ResponseEntity<Content>(newRequest, HttpStatus.OK);
		}
		
	}
	
	@PostMapping("/bike_request")
	public ResponseEntity<Content> requestToBike(@RequestBody String requestStr) {
		
		// 
		JSONObject request = new JSONObject(requestStr);
		
		long currentTimestamp = System.currentTimeMillis();
		String type = request.getString("type");
		boolean isWaitRequestComplete = request.getBoolean("wait");
		
		Content newRequest = new Content();
		newRequest.type = "bike";
		newRequest.requestType = type;
		newRequest.status = 0L;
		newRequest.createdTime = currentTimestamp;
		newRequest.lastModifiedTime = currentTimestamp;
		newRequest = contentRepo.save(newRequest);
		
		// audio request
		Content audioRequest = new Content();
		audioRequest.type = "bike";
		audioRequest.requestType = type+"_audio";
		audioRequest.status = 0L;
		audioRequest.createdTime = currentTimestamp;
		audioRequest.lastModifiedTime = currentTimestamp;
		audioRequest = contentRepo.save(audioRequest);
		
		if (isWaitRequestComplete) {
			Content requestedData;
			boolean isComplete = false;
			
			try {
				
				do {
					
					Thread.sleep(1000); // 1초에 한번씩 체크한다
					
					requestedData = contentRepo.findById(newRequest.id);
					isComplete = requestedData.status == 1L;
					
					System.out.println("Wait ... id="+Long.toString(newRequest.id));
					
				} while(isComplete);
				
				return new ResponseEntity<Content>(requestedData, HttpStatus.OK);
				
			} catch(Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Content>(HttpStatus.BAD_GATEWAY);
			}
			
		} else {
			return new ResponseEntity<Content>(newRequest, HttpStatus.OK);
		}
		
	}
	
	//
	
	@GetMapping("/cube_request")
	public ResponseEntity<List<Content>> getLatestCubeRequest() {
		List<Content> cubeRequestList = contentRepo.findByType("cube");
		List<Content> watingRequestList = new ArrayList<>();
		long currentTimestamp = System.currentTimeMillis();
		
		for (int i=0; i<cubeRequestList.size(); i++) {
			Content request = cubeRequestList.get(i);
			
			request.readCount++;
			if (request.readCount > 5L) {
				contentRepo.delete(request.id);
				continue;
			}
			contentRepo.save(request);
			
			boolean isRequested = request.status == 0L;
			if (isRequested) {
				watingRequestList.add(request);
			} else {
				if (request.lastModifiedTime < (currentTimestamp-ONE_DAY)) {
					contentRepo.delete(request);
				}
			}
		} 
		
		return new ResponseEntity<List<Content>>(watingRequestList, HttpStatus.OK);
	}
	
	@GetMapping("/bike_request")
	public ResponseEntity<List<Content>> getLatestBikeRequest() {
		List<Content> bikeRequestList = contentRepo.findByType("bike");
		List<Content> watingRequestList = new ArrayList<>();
		long currentTimestamp = System.currentTimeMillis();
		
		for (int i=0; i<bikeRequestList.size(); i++) {
			Content request = bikeRequestList.get(i);
			
			request.readCount++;
			if (request.readCount > 5L) {
				contentRepo.delete(request.id);
				continue;
			}
			contentRepo.save(request);
			
			boolean isRequested = request.status == 0L;
			if (isRequested) {
				watingRequestList.add(request);
			} else {
				if (request.lastModifiedTime < (currentTimestamp-ONE_DAY)) {
					contentRepo.delete(request);
				}
			}
		} 
		
		return new ResponseEntity<List<Content>>(watingRequestList, HttpStatus.OK);
	}
	
	@GetMapping("/all_request")
	public ResponseEntity<List<Content>> getAllRequest() {
		List<Content> allRequestList = contentRepo.findAll();
		List<Content> watingRequestList = new ArrayList<>();
		long currentTimestamp = System.currentTimeMillis();
		
		for (int i=0; i<allRequestList.size(); i++) {
			Content request = allRequestList.get(i);
			
			request.readCount++;
			if (request.readCount > 5L) {
				contentRepo.delete(request.id);
				continue;
			}
			contentRepo.save(request);
			
			boolean isRequested = request.status == 0L;
			if (isRequested) {
				watingRequestList.add(request);
			} else {
				if (request.lastModifiedTime < (currentTimestamp-ONE_DAY)) {
					contentRepo.delete(request);
				}
			}
		} 
		
		return new ResponseEntity<List<Content>>(watingRequestList, HttpStatus.OK);
	}
	
	//
	
	@PostMapping("/cube_request_complete")
	public ResponseEntity<Integer> cubeRequestComplete(@RequestBody String dataStr) {
		JSONObject data = new JSONObject(dataStr);
		
		long currentTimestamp = System.currentTimeMillis();
		int completedNumber = 0;
		
		JSONArray ids = data.getJSONArray("ids");
		for (int i=0; i<ids.length(); i++) {
			Long id = ids.getLong(i);
			Content request = contentRepo.findById(id);
			request.status = 1L;
			request.lastModifiedTime = currentTimestamp;
			request = contentRepo.save(request);
			completedNumber++;
		}
		
		return new ResponseEntity<Integer>(completedNumber, HttpStatus.OK);
	}
	
	@PostMapping("/bike_request_complete")
	public ResponseEntity<Integer> bikeRequestComplete(@RequestBody String dataStr) {
		JSONObject data = new JSONObject(dataStr);
		
		long currentTimestamp = System.currentTimeMillis();
		int completedNumber = 0;
		
		JSONArray ids = data.getJSONArray("ids");
		for (int i=0; i<ids.length(); i++) {
			Long id = ids.getLong(i);
			Content request = contentRepo.findById(id);
			request.status = 1L;
			request.lastModifiedTime = currentTimestamp;
			request = contentRepo.save(request);
			completedNumber++;
		}
		
		return new ResponseEntity<Integer>(completedNumber, HttpStatus.OK);
	}
	
	
	@GetMapping("/clear_request")
	public ResponseEntity<Boolean> clearRequests() {
		try {
			contentRepo.deleteAll();
			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.OK);
		}
	}

}
